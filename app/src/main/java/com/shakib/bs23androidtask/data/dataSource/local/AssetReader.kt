package com.shakib.bs23androidtask.data.dataSource.local

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shakib.bs23androidtask.data.model.BooksResponse
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

// Read mock data from assets to simulate API response
class AssetReader @Inject constructor(@ApplicationContext private val context: Context) {
    private fun readJsonFromAssets(): String {
        return context.assets.open("book_list.json").bufferedReader().use { it.readText() }
    }

    fun getBooks(): BooksResponse {
        val jsonString = readJsonFromAssets()
        return Gson().fromJson(jsonString, object : TypeToken<BooksResponse>() {}.type)
    }
}
