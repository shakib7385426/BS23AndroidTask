package com.shakib.bs23androidtask.data.repository

import com.shakib.bs23androidtask.data.dataSource.local.AssetReader
import com.shakib.bs23androidtask.data.model.toBookModel
import com.shakib.bs23androidtask.domain.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/*
    Repository implementation in data layer that uses the local data source to
    keep the implementation details abstract from domain layer.
*/
class LocalRepositoryImpl @Inject constructor(private val assetReader: AssetReader) : Repository {
    override suspend fun getBooks() =
        withContext(Dispatchers.IO) { assetReader.getBooks().data.map { bookDTO -> bookDTO.toBookModel() } }
}
