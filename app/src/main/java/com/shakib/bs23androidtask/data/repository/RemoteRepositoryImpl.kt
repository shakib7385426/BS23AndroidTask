package com.shakib.bs23androidtask.data.repository

import com.shakib.bs23androidtask.data.dataSource.remote.Api
import com.shakib.bs23androidtask.data.model.toBookModel
import com.shakib.bs23androidtask.domain.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/*
    Repository implementation in data layer that uses the remote data source to
    keep the implementation details abstract from domain layer.
*/
class RemoteRepositoryImpl @Inject constructor(private val api: Api) : Repository {
    override suspend fun getBooks() =
        withContext(Dispatchers.IO) { api.getBooks().data.map { bookDTO -> bookDTO.toBookModel() } }
}
