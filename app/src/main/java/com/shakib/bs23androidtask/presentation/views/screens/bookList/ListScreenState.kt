package com.shakib.bs23androidtask.presentation.views.screens.bookList

import com.shakib.bs23androidtask.presentation.model.Book

data class ListScreenState(
    val books: List<Book> = emptyList(),
    val isLoading: Boolean = false,
    val isEmpty: Boolean = false,
    val error: String = ""
)
