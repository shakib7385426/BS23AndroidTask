package com.shakib.bs23androidtask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AndroidTaskApplication : Application()
