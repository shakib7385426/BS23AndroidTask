package com.shakib.bs23androidtask.data.model

import com.shakib.bs23androidtask.domain.model.BookModel

// Data layer model
data class BookDTO(
    val id: Int,
    val image: String,
    val name: String,
    val url: String
)

// Use this function to map data layer model to domain layer model
fun BookDTO.toBookModel() = BookModel(id = id, image = image, name = name, url = url)
