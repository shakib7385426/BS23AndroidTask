package com.shakib.bs23androidtask.presentation.model

import android.os.Parcelable
import com.shakib.bs23androidtask.domain.model.BookModel
import kotlinx.parcelize.Parcelize

// Presentation layer model
@Parcelize
data class Book(
    val id: Int,
    val image: String,
    val name: String,
    val url: String
) : Parcelable

/*
    Use this function to map domain layer model to presentation layer model.
    This function is not being kept with domain layer model so that
    we don't violate clean architecture principle by injecting presentation layer dependency.
*/
fun BookModel.toBook() = Book(id = id, image = image, name = name, url = url)
