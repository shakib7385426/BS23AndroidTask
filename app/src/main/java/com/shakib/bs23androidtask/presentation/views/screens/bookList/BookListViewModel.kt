package com.shakib.bs23androidtask.presentation.views.screens.bookList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shakib.bs23androidtask.domain.usecase.GetBooksUseCase
import com.shakib.bs23androidtask.presentation.model.toBook
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookListViewModel @Inject constructor(private val getBooksUseCase: GetBooksUseCase) : ViewModel() {
    private val _state = MutableStateFlow(ListScreenState())
    val state = _state.asStateFlow()

    init {
        getBooks()
    }

    private fun getBooks() {
        viewModelScope.launch {
            _state.update { it.copy(isLoading = true) }
            val result = runCatching { getBooksUseCase() }
            result.onSuccess { books ->
                _state.update {
                    it.copy(
                        books = books.map { bookModel -> bookModel.toBook() },
                        isEmpty = books.isEmpty()
                    )
                }
            }
            result.onFailure { exception -> _state.update { it.copy(error = exception.message.toString()) } }
            _state.update { it.copy(isLoading = false) }
        }
    }
}
