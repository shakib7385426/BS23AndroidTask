package com.shakib.bs23androidtask.domain.model

// Domain layer model to keep the domain layer separate from other layers
data class BookModel(
    val id: Int,
    val image: String,
    val name: String,
    val url: String
)
