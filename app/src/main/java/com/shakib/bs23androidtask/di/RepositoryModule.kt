package com.shakib.bs23androidtask.di

import com.shakib.bs23androidtask.BuildConfig
import com.shakib.bs23androidtask.data.repository.LocalRepositoryImpl
import com.shakib.bs23androidtask.data.repository.RemoteRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    // Provides appropriate repository based on API_ENABLED flag
    @Provides
    @Singleton
    fun provideRepository(remoteRepo: RemoteRepositoryImpl, localRepo: LocalRepositoryImpl) =
        if (BuildConfig.API_ENABLED) remoteRepo else localRepo
}
