package com.shakib.bs23androidtask.presentation.views.screens.bookList

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootGraph
import com.ramcosta.composedestinations.generated.destinations.BookDetailsScreenRouteDestination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.shakib.bs23androidtask.R
import com.shakib.bs23androidtask.presentation.model.Book
import com.shakib.bs23androidtask.presentation.theme.BS23AndroidTaskTheme
import com.shakib.bs23androidtask.presentation.views.components.EmptyView
import com.shakib.bs23androidtask.presentation.views.components.ErrorView

@Destination<RootGraph>(start = true)
@Composable
fun BookListScreenRoute(
    navigator: DestinationsNavigator,
    bookListViewModel: BookListViewModel = hiltViewModel()
) {
    val state by bookListViewModel.state.collectAsStateWithLifecycle()

    BookListScreen(state = state) {
        navigator.navigate(BookDetailsScreenRouteDestination(it))
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
internal fun BookListScreen(state: ListScreenState, navigate: (Book) -> Unit) {
    Scaffold(
        topBar = {
            TopAppBar(title = {
                Text(
                    text = stringResource(R.string.book_list),
                    fontWeight = FontWeight.Bold
                )
            })
        }
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            contentAlignment = Alignment.Center
        ) {
            if (state.isEmpty)
                EmptyView()
            else
                LazyColumn(modifier = Modifier.fillMaxSize()) {
                    items(state.books) { book ->
                        BookListItemView(book = book) { navigate(book) }
                    }
                }

            if (state.isLoading)
                CircularProgressIndicator()

            if (state.error.isNotEmpty())
                ErrorView(modifier = Modifier.padding(20.dp), message = state.error)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun BookListScreenPreview() {
    BS23AndroidTaskTheme {
        BookListScreen(state = ListScreenState()) {}
    }
}
