package com.shakib.bs23androidtask.domain.repository

import com.shakib.bs23androidtask.domain.model.BookModel

interface Repository {
    suspend fun getBooks(): List<BookModel>
}
