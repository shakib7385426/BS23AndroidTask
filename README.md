# BS23AndroidTask

##  Overview
This project is developed for the sole purpose of Brain Station 23's coding test. It shows a list of books and their details. The data is fetched from `assests` folder or backend API based on the build configuration field `API_ENABLED` value.

`if API_ENABLED == true` -> fetch from backend API

`if API_ENABLED == false` -> fetch from assets folder

## Tech Stack

**Language:** Kotlin

**Architecture:** MVVM + MVI with Clean Architecture

**Concurrency:** Coroutine + Flow

**Dependency Injection:** Hilt

**Networking:** Retrofit & okhttp