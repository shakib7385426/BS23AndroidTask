package com.shakib.bs23androidtask.data.model

// Data layer model to parse API response
data class BooksResponse(
    val data: List<BookDTO>,
    val message: String,
    val status: Int
)
