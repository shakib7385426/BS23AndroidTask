plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.jetbrains.kotlin.android)
    alias(libs.plugins.parcelize)
    alias(libs.plugins.ksp)
    alias(libs.plugins.hilt)
}

android {
    namespace = "com.shakib.bs23androidtask"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.shakib.bs23androidtask"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    // Specifies one flavor dimension.
    flavorDimensions += "version"
    productFlavors {
        create("dev") {
            manifestPlaceholders += mapOf("APP_NAME" to "BS23AndroidTaskDev", "env" to "dev")
            dimension = "version"
            applicationIdSuffix = ".dev"
            buildConfigField("String", "BASE_URL", "\"https://example.com/api/\"")
            // if API_ENABLED == true -> app will fetch data from backend API
            // if API_ENABLED == false -> app will fetch data from assets folder
            buildConfigField("Boolean", "API_ENABLED", "false")
        }
        create("staging") {
            manifestPlaceholders += mapOf("APP_NAME" to "BS23AndroidTaskStaging", "env" to "staging")
            dimension = "version"
            applicationIdSuffix = ".staging"
            buildConfigField("String", "BASE_URL", "\"https://example.com/api/\"")
            // if API_ENABLED == true -> app will fetch data from backend API
            // if API_ENABLED == false -> app will fetch data from assets folder
            buildConfigField("Boolean", "API_ENABLED", "true")
        }
        create("production") {
            manifestPlaceholders += mapOf("APP_NAME" to "BS23AndroidTask", "env" to "production")
            dimension = "version"
            applicationIdSuffix = ".production"
            //signingConfig signingConfigs.production
            buildConfigField("String", "BASE_URL", "\"https://example.com/api/\"")
            // if API_ENABLED == true -> app will fetch data from backend API
            // if API_ENABLED == false -> app will fetch data from assets folder
            buildConfigField("Boolean", "API_ENABLED", "true")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.14"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.androidx.activity.compose)
    implementation(platform(libs.androidx.compose.bom))
    implementation(libs.androidx.ui)
    implementation(libs.androidx.ui.graphics)
    implementation(libs.androidx.ui.tooling.preview)
    implementation(libs.androidx.material3)
    implementation(libs.androidx.lifecycle.runtime.compose.android)
    implementation(libs.androidx.browser)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(platform(libs.androidx.compose.bom))
    androidTestImplementation(libs.androidx.ui.test.junit4)
    debugImplementation(libs.androidx.ui.tooling)
    debugImplementation(libs.androidx.ui.test.manifest)

    implementation(platform(libs.okhttp.bom))
    implementation(libs.okhttp)
    implementation(libs.okhttp.logging.interceptor)
    implementation(libs.retrofit)
    implementation(libs.retrofit.converter.gson)

    implementation(libs.hilt.android)
    implementation(libs.hilt.navigation.compose)
    ksp(libs.hilt.android.compiler)

    implementation(libs.compose.destinations.core)
    ksp(libs.compose.destinations.ksp)

    implementation(libs.coil)
}
