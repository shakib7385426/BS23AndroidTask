package com.shakib.bs23androidtask.data.dataSource.remote

import com.shakib.bs23androidtask.data.model.BooksResponse
import retrofit2.http.GET

interface Api {
    @GET("book-list")
    suspend fun getBooks(): BooksResponse
}
