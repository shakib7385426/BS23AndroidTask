package com.shakib.bs23androidtask.domain.usecase

import com.shakib.bs23androidtask.domain.repository.Repository
import javax.inject.Inject

class GetBooksUseCase @Inject constructor(private val repository: Repository) {
    suspend operator fun invoke() = repository.getBooks()
}
